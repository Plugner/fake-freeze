package me.plugner.trolls.api;

import java.util.ArrayList;
import java.util.HashMap;

import org.bukkit.entity.Player;

import me.plugner.trolls.packetinterceptor.api.PacketAPI;
import me.plugner.trolls.packetinterceptor.api.enums.PacketType;
import me.plugner.trolls.api.FreezeData;
import me.plugner.trolls.api.FreezeCallBack;

public class FakeFreezeAPI {
    private Player player,freezer;
    
    public static HashMap<Player, FreezeData> freezedPlayersData = new HashMap<>();
    public static ArrayList<Player> freezedPlayers = new ArrayList<>();

    public FakeFreezeAPI(Player player, Player freezer) {
        this.player = player;
        this.freezer = freezer;
    }

    public boolean freeze() {
        
        freezedPlayersData.put(player, new FreezeData(player, freezer, null));
        freezedPlayers.add(player);
        PacketAPI.packetListen(freezer, PacketType.IN, new FreezeCallBack());
        return true;
    }

    public boolean unFreeze() {
        if(freezedPlayers.contains(player)) {
            freezedPlayers.remove(player);
            FreezeData data = freezedPlayersData.get(player);
            // data.getThread().stop();
            freezedPlayersData.remove(player);
        }else{
            return false;
        }
        return true;
    }
}
