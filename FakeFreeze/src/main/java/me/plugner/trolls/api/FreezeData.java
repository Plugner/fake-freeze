package me.plugner.trolls.api;

import org.bukkit.entity.Player;

public class FreezeData {

    private Player player;
    private Player freezer;
    private Thread thread;

    public FreezeData(Player player, Player freezer, Thread thread) {
        this.player = player;
        this.freezer = freezer;
        this.thread = thread;
    }
    
    public void setFreezer(Player freezer) {
        this.freezer = freezer;
    }
    public void setPlayer(Player player) {
        this.player = player;
    }
    public void setThread(Thread thread) {
        thread.stop();
        this.thread = thread;
    }
    public Player getFreezer() {
        return freezer;
    }
    public Player getPlayer() {
        return player;
    }
    public Thread getThread() {
        return thread;
    }

}
