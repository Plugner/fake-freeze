package me.plugner.trolls.api;

import org.bukkit.entity.Player;

import me.plugner.trolls.packetinterceptor.api.enums.PacketType;
import me.plugner.trolls.packetinterceptor.api.interfaces.PacketBack;
import net.minecraft.server.v1_8_R3.Packet;
import net.minecraft.server.v1_8_R3.PacketPlayInBlockDig;
import net.minecraft.server.v1_8_R3.PacketPlayInBlockPlace;
import net.minecraft.server.v1_8_R3.PacketPlayInFlying;
import net.minecraft.server.v1_8_R3.PacketPlayOutEntity.PacketPlayOutRelEntityMove;

public class FreezeCallBack implements PacketBack {
    
    @Override
    public boolean onPacketReceived(Player player, PacketType packetType, Packet packet) {
                if (FakeFreezeAPI.freezedPlayers.contains(player)) {
                    System.out.println(packet.toString());
                    if(
                        packet instanceof PacketPlayInFlying.PacketPlayInLook || 
                        packet instanceof PacketPlayInFlying.PacketPlayInPositionLook || 
                        packet instanceof PacketPlayInBlockDig || 
                        packet instanceof PacketPlayInBlockPlace ||
                        packet instanceof PacketPlayOutRelEntityMove
                    ) 
                    {
                        System.out.println("BLOCKED: " + packet.toString());
                        return true;
                    }
                }
                return false;
    }
}
