package me.plugner.trolls;

import org.bukkit.plugin.java.JavaPlugin;

import me.plugner.trolls.commands.FakeFreeze;

/**
 * Hello world!
 *
 */
public class App extends JavaPlugin
{
    @Override
    public void onEnable()
    {
        this.getCommand("fakefreeze").setExecutor(new FakeFreeze());
    }
}
