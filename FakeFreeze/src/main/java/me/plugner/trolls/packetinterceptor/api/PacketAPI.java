package 
me.plugner.trolls.packetinterceptor.api;

import me.plugner.trolls.packetinterceptor.api.enums.PacketType;
import me.plugner.trolls.packetinterceptor.api.handler.PacketHandler;
import me.plugner.trolls.packetinterceptor.api.interfaces.PacketBack;

import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * @author Plugner
 * @since 1.0 (01/06/2021)
 * @apiNote This is a safe method to access {@link PacketHandler} please use that, direct requests to {@link PacketHandler} will not be supported.
 * @see PacketHandler
 * @see PacketType
 * @see PacketBack
 */
public class PacketAPI {

    /**
     * @author Plugner
     * @since 1.0 (01/06/2021)
     * @see PacketHandler
     * @see PacketBack
     * @apiNote Use this method!
     * @param player The player that's will listen the packets
     * @param packetType The type {@link PacketType}
     * @param callback Callback to in your code method
     */
    public static void packetListen(Player player, PacketType packetType, PacketBack callback) 
    {
        try 
        {
            // Trying to invoke the private method to access the API Core (I seriously don't want people using the PacketHandler directly, I'll not give support to direct requests.)
            Method handle = PacketHandler.class.getDeclaredMethod("handle", CraftPlayer.class, PacketType.class, PacketBack.class);
            // Getting the CraftPlayer to use with invoke.
            CraftPlayer craftPlayer = (CraftPlayer) player;
            // Setting handle method accessible to reflection.
            handle.setAccessible(true);
            // Finally, invoking the method.
            handle.invoke(null, craftPlayer, packetType, callback);
        } 
        catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) 
        {
            // If an error occurs, this message will be sent, the message "this is an internal error" is because this exceptions are reflections error, and not a plugin error.
            // If you seen this, please report at: https://gitlab.com/Plugner/packetlistenapi or https://www.spigotmc.org/resources/1-0-packetlistenapi-easily-use-packets-on-your-plugin.87548/

            Bukkit.getConsoleSender().sendMessage("§c§lTHIS IS AN INTERNAL ERROR, PLEASE REPORT!");
            e.printStackTrace();
            Bukkit.getConsoleSender().sendMessage("§c§lTHIS IS AN INTERNAL ERROR, PLEASE REPORT!");
        }
    }

}
