package me.plugner.trolls.commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import me.plugner.trolls.api.FakeFreezeAPI;

public class FakeFreeze implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender arg0, Command arg1, String arg2, String[] arg3) {
        if(arg0 instanceof Player) {
            if(arg3.length != 1) {
                arg0.sendMessage(ChatColor.RED + "Use: /fakefreeze <player>");
                return false;
            }
            Player player = (Player) arg0;
            Player target = Bukkit.getPlayer(arg3[0]);
            if(target == null) {
                player.sendMessage(ChatColor.RED + "This player is offline!");
                return false;
            }
            if(FakeFreezeAPI.freezedPlayers.contains(target)) {
                FakeFreezeAPI fakeFreezeAPI = new FakeFreezeAPI(player, target);
                fakeFreezeAPI.unFreeze();
                player.sendMessage(ChatColor.GREEN + "Player unfreezed successfully.");
            }else{
                FakeFreezeAPI fakeFreezeAPI = new FakeFreezeAPI(player, target);
                fakeFreezeAPI.freeze();
                player.sendMessage(ChatColor.GREEN + "Player freezed successfully.");
            }

        }else{
            arg0.sendMessage(ChatColor.RED + "[FakeFreeze] You need to be a player to use this command!");
        }
        return false;
    }
    
}
